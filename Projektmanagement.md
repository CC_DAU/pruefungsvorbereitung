## Projektmanagement 



##### Brainstorming

- Methode der Didaktik/Problemlösungsfindung
- Methode :  das freie Assoziieren von Begriffen
- Versuch, bei der Problemlösung, festgefahrene Strukturen zu durchbrechen
- Wird auch dafür genutzt, um in ein Thema einzuführen, bzw. ein Thema vorzustellen

##### Mind Mapping

- Abstrakte, komplexe Begriffe sollen verstanden werden
- Methode: Der zentrale(abstrakte) Begriff wird visuell mit damit assoziierten weiteren Begriffen solange Verknüpft, bis eine konkrete, verstandene "Landkarte" des Begriffzusammenhangs ensteht
- Regel : Jeder Begriff auf der Mindmap darf nur einmal vorkommen



##### Oranigramme

- Hirarchische Strukturen/Organisationsstrukturen) lassen sich mit Organigrammen sehr gut und verständlich darstellen

##### Kompetenzen

- Typen und Formen von Kompetenzen:
  - Fachkompetenz :
    - Fachlich bezogenes Wissen
  - Sozialkompetenz:
    - Summe aller Fähigkeiten, welche einem Menschen ermöglichen, in sozialen Situationen	  angemessen und konstruktiv zu agieren
  - Methodenkompetenz:
    - Je systematischer und strukturierter man an die Lösung eines konkreten Problems herangeht, umso höher ist die Methodenkompetenz
  - Personale Kompetenz:
    - Bezogen auf die eigene Person
    - (intrinsische)Motivation
    -  Pünktlichkeit
  - Handlungskompetenz
    - Die Summe aller vorher genannten Kompetenzen
  - Schlüsselqualifikation
    - Handlungskompetenz - Fachkompetenz


##### Release Management:

- Die Planung, Steuerung und Verwaltung von angefragten und autorisierten Änderungen an einer IT-Struktur
- Request for Change
- Arten von Änderungen: Fehlerbehebungen, Problembehebungen, Gewünschte Features
- Ziel ist die systematische Überwachung von Rollouts



##### Aufgaben eine Projektleiters

- Planung
- Steuerung
	
- Überwachung

- Budgetierung
- Ressourcenmanagement
- Kommunikation nach innen(Team) und aussen(Auftraggeber)
- ....



##### Auftragsabwicklung

- Abarbeitung von Kundenaufträgen

  		1. Auftragsübermittlung
        		
        		2. Auftragsbearbeitung
        
    			- Neukunden Aufnehmen
      			- Kundenprüfung
                    			- Preiskonditionen
                			- ...
    
    		3. Fertigung/Zusammenstellung
    	    
    			- Kommissionierung
    	    
    		4. Verpackung und Versand
    	    
    			- Kundenabnahme
    	    
    		5. Fakturierung
    	    
    			- Rechnung wird erstellt
    	    
    		6. Bezahlvorgang



##### Projekte

- Wann spricht man von einem Projekt

			a) Einmaliges Vorhaben
			b) Zeitlich eingegrenzt
			c) Klar definiertes Projektziel
- Komplexität von Projekten
- Grundzüge des Controllings
- Woran können Projekte scheitern?
  - Kommunikation
  - Schlechte Kalkulation/betriebswirtschaftliche Faktoren
  - Mangelnde Ressourcenverfügbarkeit
- Unterscheidungsmerkmale von Projekten
  - In der Größenordnung
  - Branche
  - Grad der Einmalig



##### 	EULA(End User Licence Agreement)

- Lizenzvereinbarung, welche die Nutzung einer Software regelt
- Was wird geregelt?
  - Die Haftung
  - Copyright
  - Art und Weise, wie die Software benutzt werden darf


##### Projektmanagement

- Wodurch unterscheiden sich Projekte :

  - Grad der Einmaligkeit
  - Komplexität
  - Branche
  - Projektinhalt

  

- Was sind dieAufgaben des Projektmanagements :

  - Planung
  - Controlling
  - Qualitätssicherung
  - Kommunikation
  - Motivation



- Was erhöht die Komplexität von Projekten :

  - Steigende Anzahl an Mitarbeitern
  - Teile des Projektskönnen nicht überblickt bzw vollständig verstanden werden

  

- Projektarten :

  - Interne und externe Projekte



- Anforderungsmanagement als Teil des Projektmanagements :
  - funktionale
    - echte anforderung 
  - nicht-funktionale
    - "ansprechendes design"
    - bedienbarkeit für behinderte (barrierefrei)
  - kritische
    - werden durch die muss-kriterien definiert
    - definierenalso kernfunktionen eines it-systems
  - Welche Merkmalen müssen erhobene Anforderungen entsprechen :
    - klar und verständlich formuliert
    - die SMART-Regel
  - Die Projektplanung : 
    - Die Planungsansätze Top-Down und Bottum-Up
      - Topdown -> vom großen Problem runter zu so kleinen bis die lösbar sind: abstrakt zu konkret
      - Bottum-Up -> konkret zu abstrakt
      - -> Best Practises (Was hat sich bisher bewährt)
  - Bennen Sie die Ebenen der Projektplanung
    - Bei jedem Planungsschritt wirdentschieden, wie weit in die Zukunft geplant wird und wie sehr ins Detail herunter gebrochen wird
      - Planungstiefe
        - Wie sehr gehe ich ins Detail
      - Planungshorizont
        - Wie weit plane ich in die Zukunft
  - Was sind die Ziele der Projektplanung 
    - Für die Schaffung von systematischen, handlungsleitenden Durchführungsgerüst
    - Führungsinstrument
    - Basis für Verhandlungen
  - Womit/Wann beginnt die Projektplanung :
    - Mit Auftragserteilung
- Der Projektauftrag : 
  - Die wichtigsten Inhalte eines Projektauftrages:
    - Beschreibung des Projekts
    - Projektziel
    - Zeitraum
    - Ressourcen -> Budget
    - Projektbeteiligte
    - Risiken
    - Auftraggeber
- Kick-off-meeting

###### Welche Aufgaben haben Projektmodelle

- Festlegung von Vorgängen im Projekt, des Ablaufs...
- Festlegung einer Arbeitsweise
- Teilaufgaben definieren
- Schnittstellen definieren
- Reduzierung der Komplexität
- Art und Weise des Testens definieren
- Entscheidungspunkte schaffen

###### Was sinddie Ergebnisse der Projektplanung

- Ein Projektplan
- Pflichtenheft
- Fachkonzept

###### Was sind die wesentlichen Inhalte des Projektplans

- Aufwandsschätzung bezüglich einzusetzender Ressourcen

###### Schätzungen des Aufwandes in Projekten

- Schätzungen sind erst einigermaßen realistisch, wenn alle Anforderungen nach SMART-Regel erfasst wurden
- Eine jede realistische Schätzung von Aufwand in Projekten beruht darauf, dass man eine klare Vorstellung aller Arbeitspakete im Projekt hat.
- Welche Methoden der Aufwandsschätzung in Projekten kennen Sie
  - Zwei-Zeiten-Methode(Min-Max-Schätzwert) / 2
- Gruppenschätzung
- Einzelschätzung

###### Was für Pufferarten kennen Sie in der Projektplanung

- Freier puffen
  - immer bezogen aufden direkt darauf folgenden Arbeitsschritt
- Gesammtpuffer
  - immer bezogen auf das Projektende

###### Wozu dienen Meilensteinein Projekten? 

- Das Projekt aufteilen in überprüfbareEtappen ->controlling
- Motivation (erfüllte Erwartungshaltung) -> psychologische Komponente
- Teilerfolg im Projekt

###### Woraus besteht die Kostenplanung in einem Projekt

- Kostenplan, welcher auf der Kostenschätzung - bezogen auf einzelne Arbeitsschritte - beruht

###### Nennen Sie typische Risikofaktoren in Projekten

- Zeit
- Kosten/Budget
- Personal
- Kommunikation
- Fachwissen
- Rechtliche Risiken
- Technologie

###### Wozu dienen Projektphasen

- Struktorierung
- komplexe Sachverhalte aufschlüsseln
- Überblick, Übersicht

###### Logische Abfolge der klassischen Phasen in Projektmodellen

0. Qualifikationsphase
1. Analyse
2. Design
3. Implementierung / Realisierung
4. Inbetriebnahme / Abnahme

5. Betrieb

###### Was sind die wichtigsten Inhalte des Pflichtenheftes

- Zielbestimmungen
- Produkteinsatz
- Produktumgebung

- Produkfunktionen -> Anforderungen aus Sicht des Endusers/Nutzers
- Daten/ Datenhaltung
- Leistungsanforderungen
- Qualitätsbestimmungen
- Produktivumgebung / Entwicklungsumgebung

##### Welche Aufgaben hat das Controlling

- Vergleich Ist-Soll
- Abweichungen melden -> Meldewesen
- Korrekturmaßnahmen einleiten



##### Nutzwertanalyse

<img src="Bilder/Nutzwertanalyse.png" />



- Gewichtung festlegen, Noten vergeben, Bewertung ist die Summe aus den Produkten (Note*Gew)
- 


