## BWL & WISO 

[TOC]



##### Grunbegriffe der BWL

- Ertrag:

  - Werterhöhung des Nettovermögens

- Nettovermögen:

  - Vermögen - Schulden = Eigenkapital

- Aufwand:

  - Alles, was das Nettovermögen mindert

- Unterschied zwischen Kosten und Aufwand:

  - Kosten: Leistungsspezifisch(beziehen sich immer nur auf die betriebliche Leistungserbringung)
  - Aufwand/Aufwendungen: Übergreifend

- Fixkosten und variable Kosten:

  - Fixkosten: Alle Kosten, welche bei der Leistungserbringung konstant bleiben
  - Variable Kosten: Alle Kosten, welche sich bei der Leistungserbringung ändern können
  - Gesamtkosten = Fixkosten + variable Kosten

- Wirtschaftlichkeit:

  - Ertrag / Aufwand : Wenn das Ergebnis >1, dann macht die Firma Überschuss(Jahresüberschuss)

- Umsatz/Umsatzerlöse:

  - Summe der Verkaufserlöse(betriebsspezifisch)
  - Umsatz = Absatzmenge * Preis

- Schmälerungen:

  - NICHT Teil des Nettoumsatzes sind die Schmälerungen
    - Alles außer Umsatzsteuer -> Rabatt, Skonto...
    - Nettoumsatz = Umsatz - (Umsatzsteuer + Schmälerungen)

- Gewinn:

  - Im Gegensatz zum Umsatz werden hier die Kosten/Aufwendungen abgezogen

- Produktivität:

  - Bezug auf die Menge


##### Betriebliches Vorschlagswesen

- Das **betriebliche Vorschlagswesen**
  (BVW) auch „Verbesserungsvorschlagswesen“ (abgekürzt: VV-Wesen) ist ein
  partizipatives (mitarbeitereinbeziehendes) Optimierungssystem mit dem 
  Ziel, das Ideenpotenzial aller Mitarbeiter (nicht nur das der Manager 
  und Experten) in einer Organisation zu nutzen.



##### Arbeitsrecht 

- Unfallverhütung
- Mutterschutz
- Arbeitszeitregelungen
- Urlaubsregelungen
- Jugendarbeitsschutzgesetz



##### Betriebsrat

- Aufgaben eines Betriebsrats:
  - Mitbestimmen(Lohn, Urlaub, Unfall, Arbeitszeit)
  - Mitwirken
  - Informieren/Beraten
  - 

##### Märkte

- Auf Märkten werden Güter unter Preisbildung gehandelt
- Was ist ein Gut?
  - Ein Mittel zur Befriedigung eines Bedürfnisses

- Ökonomische Prinzipien :
   - Maximalprinzip
   - Minimalprinzip

- Markformen

  - Die Anzahl der Anbieter auf der einen und der Nachfrager auf der anderen Seite, bestimmen
    			   die Marktform: Monopol, Oligopol, Polypol....

- Der "vollkommene Markt"

    	1. Der Markt ist für jeden offen
     2. Weder Anbieter, noch Nachfrager werden bevorzugt/benachteiligt
     3. Vollkommene Markttransparenz
     4. Alle Güter sind von gleicher Güte(gleichwertig)
     5. Markt reagiert schnell auf Änderungen

- VWL
  - BIP(Bruttoinlandsprodukt) -> Wert aller Güter+Dienstleistungen eines Jahres

- Marktdurchdringung:
  - Grad der Verbreitung/Bekanntheit innerhalb eines etablierten Marktes bzw. Verbrauchergruppe
- Marktentwicklung:
  - Grad der Verbreitung mit neuen Produkten auf etablierten Märkten
- Diversifikation:
  - Ausbrechen aus dem angestammten Geschäftsfeld


##### Corporate Identity

- Die Art und Weise, wie eine Organisation sich nach innen und nach aussen hin selbst präsentiert

  ###### 3 Säulen der Corporate Identity:

  1. Corporate Design
  	
  	- Logo, Farben...
  	
  	- Vorschriften zur Darstellung
  	- Produktdesign
  	- Architektur, Raumgestaltung

  2. Corporate Behaviour

  	- Verhalten gegenüber Kunden
  	- Verhalten gegenüber Mitarbeitern
  	- Dresscode
  	- Firmenmotto
  	- Verhalten von Vorgesetzten, Mitarbeiterführung
  	- Verhaltenscodex
  	
  3. Corporate Communication

  	- Pressesprecher
  	- Werbung?
  	- Auftreten auf Messen
  	-> Public Relations...


##### Geschäftsbeziehungen

- B2B(Business-to-Business)

  			-> Geschäftsbeziehungen zwischen Unternehmern/Unternehmen
        			
        			-> Marketing ist ein besonderes(nicht auf Privatleute zugeschnitten)
        			
        			-> Vertragsrecht:
        			
     
        				- Schriftform ist nicht zwingend
                     - Keine Stornierung
                     - Angebote sind ohne Freizeichnungsklauseln bindend       				
                  
                  
                    				- Kaufmännische Bestätigungsschreiben sind nach 2 Wochen Schweigen bindend



- B2C(Business-to-Consumer)

			- Verbraucherschutz
			
			- Widerrufsrecht und Rücktrittsrecht


- B2G(Business-to-Government)

			- Firmen-Behörden-Beziehungen
			
			- Lobbyarbeit
			
			- Ausschreibungen

  ######  Einliniensystem

<img src="Bilder/einlinien.png" />

**Vorteile:**

- Schnelle Kommunikation und Entscheidungsfindung. Entscheidungen werden einfach von „oben nach unten“ durchgereicht.
- Klare Kompetenzen. Die Hierachie ist eindeutig. Es gibt keine  Überschneidungen bezüglich der Weisungsbefugnisse, da jede  Organisationseinheit genau eine übergestellte hat. Damit wird ein  „Kompetenzgerangel“ verhindert.

 

**Nachteile:**

- Entscheidungsträger können unter Umständen überfordert werden, da  jede Entscheidung von Ihnen bestätigt werden muss. Dies führt bei  Überlastung zu höheren Durchlaufzeiten.
- Je mehr Organisationsebenen es gibt, desto größer werden die  Nachteile der Einlinienorganisation: mit jeder Ebene dauert es länger  bis eine Weisung oder Information die Kette durchlaufen hat. Zudem  besteht die Gefahr, dass Informationen verfälscht werden oder verloren  gehen („Stille Post-Problem“).
- Da Entscheidungen von oben nach unten weitergereicht werden, müssen  die Mitarbeiter Disziplin und Gehorsam an den Tag legen. Dies hemmt  Kreativität und die persönliche Entfaltung der Mitarbeiter.

   ###### Mehrliniensystem

<img src="Bilder/mehrlinien.png" />

  		-> Jeder MA kann mehrere Vorgesetzte haben

**Vorteile:**

- Entlastung von Vorgesetzten: beim Mehrliniensystem werden  Entscheidungsträger im Vergleich zum Einliniensystem entlastet, da  Organisationseinheiten mehreren Vorgesetzten unterstellt sind. Dadurch  können die übergeordneten Stellen durch Arbeitsteilung entlastet werden.
- Spezialisierung von Vorgesetzten: da es mehrere Vorgesetzte gibt,  kann sich jeder Vorgesetzte auf einen Fachbereich spezialisieren.
- Direkte Informations- und Kommnukationswege: während beim  Einliniensystem starre Kommunikationswege eingehalten werden, können  beim Mehrliniensystem kürzere direkte Kommnunikationswege  („Abkürzungen“) genutzt werden.
- Bessere Kontrolle von Mitarbeitern: da es mehr Vorgesetzte gibt, kann die Arbeit von Mitarbeitern besser kontrolliert werden.

**Nachteile:**

- Durch Überschneidung von Kompetenzen, kann es zu  Kompetenzstreitigkeiten kommen, bei denen sich mehrere Vorgesetzte für  die selben Aufgaben zuständig fühlen.
- Untergeordnete Organisationseinheiten können unter Umständen von  verschiedenen Vorgesetzten unterschiedliche Anweisungen bekommen. Dies  kann zu Verwirrung, Missverständnissen und Konflikten führen.

​		  ​					
###### Stabliniensystem

  **Vorteile:**

- Bei wachsenden Unternehmen kommt es beim Einliniensystem zu  Überforderung der Entscheidungsträger. Die Stabstellen sollen  Vorgesetzte entlasten und durch ihr Expertenwissen kompetente  Entscheidungen ermöglichen
- Qualität der Entscheidungen erhöht sich
- Ansonsten gelten die Vorteile des Einliniensystems

**Nachteile:**

- Höhere Kosten: Die Stabsstellen müssen durch qualifizierte Experten besetzt werden. Dies erhöht die Personalkosten
- Abhängigkeit: Auch wenn Stabsstellen keine Entscheidungsbefugnisse  haben, können Vorgesetzte von dem Spezialwissen der Experten abhängig  werden („Macht der Experten“)

  

##### Formen der Untenehmensgliederung

###### Funktionale Organisation:		

<img src="Bilder/funktional.png" /> 

	Unter funktionaler Organisation (Verrichtungsorganisation) versteht man eine Gliederung der Einheiten einer Organisation nach Aufgaben (z. B. Produktion, Vertrieb, Verwaltung) auf der zweiten Hierarchieebene unterhalb der Unternehmensleitung.
	
			- Unternehmen wird nach deren einzelnen Funktionen gegliedert(IT, Einkauf, 				  Personalmanagement...)
			
			- Grundlage: In aller Regel Einlinien- und Stabliniensystem
######  Divisionale Organisation:		

<img src="Bilder/divisional.png" />

			- Gliederung nach Geschäftsbereichen(Sparten)
			
			- Jede Sparte ist im operativen Geschäft eigenverantwortlich
			
			- Geschäftsführung trifft lediglich strategische Entscheidungen
###### Matrixorganisation:	

<img src= "Bilder/matrix.png" />

			-> Verbindung von funktional und divisional
			Vorteile:
	        	- kurze Kommunikationswege
	        	- keine Hierachie
	        	- spezialisiertes Führungspersonal
	        	- stärkere Teamarbeit
	        	
	        Nachteile:
	        	- mehr Führungspersonal wird benötigt
	        	- mehr Konflikte und Konkurrenzkämpfe im Team
	        	- mögliche Überforderung der Mitarbeiter
	        	- erhöhter Kommunikationsbedarf
			

##### Personalkosten

- Gehälter(Konstante monatliche Zahlungen, Leistungsprämien)
- Löhne(Gekoppelt an die reine Arbeitsleistung, Zeitlohn)
  - Beides historische, heute überholte Begriffe
- Sozialabgaben


##### Deckungsbeitrag

- Deckungsbeitrag = Umsatzerlöse - variable Kosten
  - Ziel ist, sich einen Überblick über die Deckung aller Produkte zu verschaffen



##### Ereignisgesteuerte Prozessketten

Die **Ereignisgesteuerte Prozesskette** (**EPK**)
ist eine grafische Modellierungssprache zur Darstellung von 
Geschäftsprozessen einer Organisation bei der 
Geschäftsprozessmodellierung / Darstellung der Prozessorganisation 
(Abläufe und Arbeitsschritte) bei der Unternehmensabbildung.



<img src="Bilder/epk.png" />



##### Distributionswege:

- Indirekter Absatz
  - Zwischenhandel, Aussenhandel....
- Direkter Absatz
  - Verkaufsstände, Haustürgeschäfte ...
    





##### Mängel

- Arten von Mängeln:
  - Sachmängel
    - Schäden an der Ware
    - Fehlende/nicht vorhandene Funktion
    - Mangel in der Art
    - Mangel in der Menge
  - Rechtsmängel
    - Wenn eine Eigenschaft, die Herkunft oder die Beschaffenheit einer Ware gegen geltendes		  Recht verstößt



##### Urheberrechtsgesetz

- Was wird hier geschützt?
  - Geistiges Eigentum
- Wer ist der Urheber?
  - Der Schöpfer
- Schöpfung muss sich physisch manifestiert haben
- Es muß nicht beantragt werden
- 70 Jahre nach dem Tode erlischt es
- Kann vererbt und verlängert werden
- *Was nicht geschützt ist: Die reine Idee*

##### Patentrecht:

- Muss man beantragen
- Gewerbliches Schutzrecht für technische Erfindungen



##### Marktformen

- Monopol 
- Oligopol
- Polypol -> vollständige Konkurrenz

###### Käufermarkt

- Angebot > Nachfrage(Angebotsüberhang)

###### Verkäufermarkt

- Verknappung von Gütern
- Angebot < Nachfrage

###### Vollkommener Markt

- 100% Transparenz
- Markt reagiert sehr schnell auf Änderungen
- Homogen
- In der Realität nicht existent

###### Unvollkommener Markt

- Güter sind verschieden, haben verschiedene Güte
- Markt ist intransparent, reagiert träge



##### Leasing

- Eine spezielle Form der Miete
- Leasing bietet Spielräume in Bezug auf Verwertung, Risikohaftung und Nutzung
- Wann lohnt sich Leasing?
  - Abschreibungsfähigkeit
  - Wie hoch sind die Leasinggebühren
  - Vergleichen mit Alternativen(Kauf, Miete, ..)

##### Gesamtbetriebskosten(TCO, Total cost of ownership)

- Anschaffungskosten inklusiv aller im laufenden Betrieb anfallenden Kosten 
  - Wartung
  - Sicherheit
  - Reperatur
  - Stromkosten
  - Reinigung
  - ...



##### Telemediengesetz

- Regelt die Rahmenbedingungen von Internetangeboten
- Mindestangaben auf einer Internetseite:
  - Impressum
  - AGB
  - Datenschutzhinweise....


##### Affiliate-Systeme

- Geschäftsmodell

- Vertriebsart im Internet

- Anbieter kann bezahlt werden:
  - per click
  - per view
  - per sale ...
- Vorteile:
  - Erfolgsabhängige Werbung
  - Gezielt
- Nachteile:
  - Möglicher Imageschaden durch deplazierte Werbung
  - Oft mangelnde Transparenz zum Anbieter



##### Gewährleistung und Garantie

- ###### Gewährleistung

			-> Rechtliche Verpflichtung
			
				-> Wer verpflichtet sich?
				
					-> Die Händler und die Hersteller
					
				-> Wozu verpflichten die sich?
		
					- Ware muss innerhalb bestimmter Fristen frei von Mängeln sein
					
					- Gewährleistungsfrist: 24 Monate
					
					- Für Gebrauchtwaren: 12 Monate
					
				-> Welche Rechte hat der Konsument bei Mängeln?
				
					- Nacherfüllung
					- Rücktritt
					- Schadenersatz
					- Minderung
		- Garantie
		
			-> Freiwillige, zusätzliche Vereinbarung der Hersteller/Händler
			
			- Keine Gewährleistung!
			- Inhalte der Garantie dürfen nicht gegen Gewährleistung verstoßen


​	

##### Materialwirtschaft

- Mindestbestand / Sicherheitsbestand 

  - die Menge einer bestimmten Ware die im Lager nicht unterschritten werden darf
  - Notreservetage (Puffer) -> Informationen im Text
  - Notreserve * AVG (Tagesverbrauch) = Mindestbestsand

  

- Meldebestand 

  - Derjenige Bestand, welcher die Unterschreitung des Mindestbestandes verhindern soll
  - AVG * Beschaffungszeit + Mindestbestand = Meldebestand

  

- JIT (Just in Time)  



- Maximalbestand
  - Anzahl einer Ware, welche nicht überschritten werden darf
  - Mindestbestand + optimale Bestellmenge = Maximalbestand



##### Arbeitsproduktivität

- Produktionsleistung / Anz. Mitarbeiter bzw. Maschinen



##### Aufbauorganisation:

- Wie ist ein Unternehmen strukturiert?

- Formen der Aufbauorganisation:




##### Kapazitätsauslastung

- Kapazität :  Maximal produzierbare Menge eines Gutes
- Hängt ab von Anzahl Maschinen / Mitarbeiter

- Die größe des verfügbaren Lagers ist ebenfalls ein bestimmender Faktor

- Produktionsstunden / Kapazitätsstunden * 100 = Kapazitätsauslastung in Prozent





##### Vollmachten & Prokura



<img src="Bilder/vollmachtenProkura.png" />





##### Breakeven-point

- Punkt, an welchem Erlös = Kosten

- Gewinnschwelle



###### Aufgabenvarianten

1.  Mindestumsatzmenge = Fixkosten / Deckungsbeitrag
2. Gewinnschwelle = Fixkosten / Deckungsbeitrag * Listenpreis



##### Berufsgenossenschaft

- Träger der gesetzlichen Unfallversicherung
- 



##### Tarifvertrag 

- Verträge zwischen Arbeitnehmer und Arbeitgeber(verbänden)

- Was regelt ein Tarifvertrag?

  - Löhne/Gehälter
  - Kündigungszeiten
  - Arbeitszeiten
  - Urlaubstage

  <img src="Bilder/Tarifvertrag.png" />

  ​										Beispiel dafür, was im Tarifvertrag stehen kann

##### Outsourcing / Outplacement




##### Wirtschaftssektoren

-  primärer Sektor (Urproduzent)
  - Rohstoffgewinn
- sekundärer Sektor (Weiterverarbeitung)
  - alle betriebe, welche die rohstoffe aus dem primären Sektor weiterverarbeiten
- tertiärer sektor (Handel)



###### Nach welchen Kriterien lassen sich Betriebe einteilen

- Nach Branche/Wirtschaftszweig
- Nach Größe des Betriebs
- Nach Rechtsform



##### Einteilung von Betrieben nach Rechtsform



<img src="Bilder/Rechtsformen.png" style="zoom: 150%;" />

<img src="Bilder/Rechtsformen2.png" />




##### BWL



		- Wichtigste Punkte des Jugendarbeitsschutzgesetzes
		
			 - (Duale Ausbildung)
			
		- Mutterschutz


​		
​		- Ablauforganisation
​		
​			-> Organisation von Arbeitsprozessen


​		
​		- Umweltschutz
​	
​		- Brandschutz
​		
​		- Unfallverhütungsmaßnahmen
​		
		- Arten von Geschäftsprozessen
		
			- Managementprozesse
			
			- Wertschöpfungsprozesse
			
			- Schlüsselprozesse


​			
​		- Sozialversicherungen
​		
​		- Betriebsrat




###### Kostenstellenrechnung

<img src="Bilder/kostenrechnung.png" />

 

