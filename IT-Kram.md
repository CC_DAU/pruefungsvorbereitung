
## IT-Wissen

[TOC]




##### Backups

###### Grundlegende Strategien:

###### 	Full Backup


				- Alle zu sichernden Daten werden hier abgelegt
				
				- Vorteile
				
					- Jedes Backup hat alles, unproblematisch bei Verlust
					
					- Simple Wiederherstellung
	            - Nachteile
				
					- Hoher Speicherbedarf
					
					- Hohes Datenaufkommen
					
					- Zeitaufwändig


​				

	###### Differenzielle Backups
	
				- Immer bezogen auf das letzte Full Backup
				
				- Was wird gespeichert?
				
					- Alle Änderungen seit dem letzten Full Backup
				- Vorteile
					- Zeitersparnis pro Backup
					- Reduzierter Speicherbedarf
					
				- Nachteile
					- Abhängig von den zu sichernden Daten, kann sich Diff-Backup auch sehr 				  negativ darstellen				



###### Inkrementelle Backups

				- Hier wird als Bezugspunkt der zu sichernden Änderungen das letzte Inkrement 				genommen
				
				- Alle Teilbackups für die Recovery nötig
				
				- Für eine vollständige Wiederherstellung braucht man das letzte Full Backup 			   und alle seitdem erstellten Inkremente


##### Grundbegriffe Anwendungsentwicklung 

- Compiler
- Interpreter





##### Skriptsprachen

- Ursprünglich dafür entwickelt, um in speziellen Nischen schnelle Lösungen coden zu können
  - "Wenig Code, großer Effekt"
  - Spezialisierter Befehlssatz
  - Schwache bis keine Typisierung(damit man schnell entwickeln kann)
- In aller Regel Interpretersprachen



##### XML

- Auszeichnungssprache
- Dient der flexiblen Beschreibung von Datenstrukturen

##### HTML

- Was ist neu ab HTML5?
- Grundgerüst einer HTML-Seite
- 


##### CMS(Content Management Systems)		

- Software, welche flexibel und ohne Fachkentnisse die Möglichkeit bietet, Content bereitzustellen und
            zu verwalten

##### Von-Neumann- Architektur



<img src = "https://www.elektronik-kompendium.de/sites/com/bilder/13092611.gif" title="Von-Neumann" />

- Rechenwerk (ALU)
- Steuerwerk(Control Unit)
- Speicherwerk(Memory)
- Ein-/Ausgabewerk (I/O-Unit)


##### Multitasking

- Präemtives Multitasking:
  - Rechenzeit vom OS gesteuert verteilt
  - Komplexer Scheduler

- Kooperatives Multitasking:
  - Scheduler kooperiert mitden "Bedürfnissen "der Prozesse
  - Einfacher Scheduler



##### Grafikformate

- Wichtigste Formate inklusive Haupteigenschaften
  - Wieviele Farben?
  - Komprimierung?
  - Wo praktisch im Alltag eingesetzt?

###### Speicherbedarf einer Grafik berechnen 

- Farbtiefe 

  - Anzahl Bits, welche für die Kombination der Farbinformation verwendet werden können

- Speicherbedarf:

  - Auflösung -> 800*600 Pixel

  - Farbtiefe -> 24 Bit

    Berechnung : 800 * 600 * 24 = Anzahl Bit

    

##### Betriebssysteme

- Eine Software die die grundlegendsten Hardware-Ressourcen eines Rechners nutzbar macht und verteilt/zuweist
- Nennen Sie Hauptaufgaben eines OS!
  - Dateiverwaltung
  - Speicherverwaltung
  - Fehlerbehandlung
  - Prozesssteuerung

##### Dateisysteme

- Ablagemanagement für Dateien
- Datenträger werden dafür auf bestimmte Art und Weise logisch struktoriert
- Attribute der einzelnen Dateien und deren Metadaten müssen verwaltet werden



##### Polling und Interrupts


##### Struktorierte Verkabelung

- Primäre Verkabelung
  - verbindet Gebäude
  - Glasfaser
- Sekundäre Verkabelung
  - verbindet Etagen
  - Glasfaser
- Tertiäre Verkabelung
  - Raumverkabelung/ horizentale Verkabelung
  - Kupfer








##### Routing

- Wegefindung für die Weiterleitung von Datenpaketen über das Netzwerk
- Welche Aufgaben haben Router?
  - Routing
  - Verbinden Netzwerke
- Minimale Konfig-Einstellungen eines Routers:
  - IP-Adresse
  - Subnetzmaske
  - Gateway
- Netzwerkklassen -> CIDR (Classless Inter-Domain Routing)



##### Netzwerksicherheit

- Nennen Sie die Kriterien der NW-Sicherheit!

  - Authentifizierung
  - Authorisierung
  - Vertraulichkeit
  - Verfügbarkeit



##### Ports (Netzwerke)

- Teil einer Netzwerkadresse
-  Well-known-Ports
- Reservierte Ports 
- Socket -> Softwareseitig aufgebaute Verbindung mit Adresse+Port
- Dienstzuordnung 

<img src="Bilder/ports.png" />






##### 	ERP?

		- Vorteile
		
			- Ist oft Open Source bzw. kostengünstig
			- Ohne Programmierkenntnisse kann man Content bereitstellen
			- Trennung zwischen Front End und Back End
			- Anwender kümmert sich um den reinen Content, Fachleute um die Verwaltung


​		
​		- Nachteile
​		
​			- Oft so komplex, dass man Einarbeitung/Schulung benötigt
​			- Man ist eingeschränkt auf die Funktionen, welche einem das CMS anbietet









##### DMZ(De-militarisierte Zone)

- Eigenständiger Netzwerkbereich, welcher vom Restnetzwerk durch mindestens ein Firewallsystem		  abgegerenzt wird

- Dient dem Schutz von IT-Strukturen gegenüber dem öffentlichen Telekommunikationsnetz und dem Intranet


##### Virtualisierung:

- Abstraktion von physikalisch gegebener Hardware
- Ressourcen logisch zusammenfassen
  - Gegebene hardware kann mehrfach genutzt werden
  - Immer bei knappen Ressourcen, effektive Hardwarenutzung
- Hypervisor
- Emulation
  - Nachbildung einer kompletten Hardwarearchitektur softwareseitig
  - Kostet Ressourcen, sehr aufwändig in der Entwicklung
  - Performancelastig

  
  
##### Versionskontrolle

- Wichtige Grundbegriffe:

			-> Repository: Hier liegen alle Programmversionen/Dokumentversionen
			
			-> Branch: Voneinander unabhängie Entwicklungslinien
			
			-> Fork: Abspaltung von einem bestehenden Projekt
			
			-> Master/Trunk: Hauptentwicklungslinie
			
			-> Merge: Zusammenführung verschiedener Branches
			
			-> Head: Aktuellste Variante in einem Branch
			
			-> Commit: Einfliessen einer Änderung/von Änderungen in ein Repository
		
			-> ....



  

##### Bundesdatenschutzgesetz

- Die Erhebung, Verarbeitung und Speicherung personenbezogener Daten
- Was sind personenbezogene Daten?
  - Alle Daten, über welche eine Person bestimmbar ist
  - Summe aller Daten, welche eindeutig einer Person zugeordnet werden können
- Das Persönlichkeitsrecht der informationellen Selbstbestimmung wird hier im Kern geschützt
- Welche Rechte hat man bezogen auf diese Daten?
  - Löschung
  - Auskunftsrecht
  - Berichtigung
  - Sperrung
  - Widerspruch

##### Grundbegriffe der Datensicherheit

- Vertraulichkeit
  - Information wird vor unbefugtem Zugriff geschützt
- Verfügbarkeit
  - Ein Dienst/eine Info/eine Funktion ist zu einem oder zu bestimmten Zeitpunkten verfügbar
- Integrität
  - Daten liegen unverändert/nicht manipuliert bzw. vollständig vor
- Authentisierung
  - Verifizierung der Identität -> Die Authentifizierung erfolgt daraufhin passiv implizit
- Authorisierung
  - Wenn man also authentisiert und verifiziert ist, weist das System einem die ensprechenden		   Rechte zu
- Datenschutz
  - Schutz personenbezogener Daten
- Datensicherheit
  - Setzt konkret systemisch die oben genannten Punkte um (bis auf den Datenschutz)



##### Angriffsformen

- Social Engineering
  - Sammeln von personenbezogen Daten mit dem Ziel, eine Art Profil anzulegen, oft Teil bzw. erste Phase eine mehrstufigen Angriffes
  - Formen des Social Engineerings:
    - Phishing
    - Hoaxes
- Denial of Service(DoS)
  - Überlastung eines Systems durch massenhafte Anfragen
- DDoS
  - Koordiniert durchgeführter DoS
- Spoofing
  - Das Ändern einer Quelladresse in Datepaketen
- Sniffing
  - Aufzeichnung von NW-Verkehr
- Replay
  - Das Zurückspielen von vorher aufgezeichnetem und manipuliertem NW-Verkehr in das Netzwerk
- Man-in-the-Middle
  - Angreifer greift Kommunikation ab und sendet die Pakete manipuliert an das Ziel
- Poisoning
- Port-Scans



##### IT-Sicherheit

- Was versteht man in der IT unter Sicherheit? Wann ist ein IT-System sicher?
  - Ein System ist dann sicher, wenn es genau das tut, wofür es vorgesehen ist und nichts 				  anderes



##### Angriffsformen auf Anwendungen

- SQL Injection
- Cross-Site-Scripting



##### Gehärtete Systeme

- Systeme, bei denen :
  - immer alle aktuellen Updates eingespielt sind
  - allenicht notwendigen Dienste deaktiviert/deinstalliert sind



##### Fehlertoleranz

- Je selbständiger ein System mit aufgetrenen Fehlern umgehen kann, umso fehlertoleranter ist es

- Methoden der Fehlertoleranz:

  				- Fehlererkennung
  				- Fehlerkorrektur
  				- Fehlervermeidung
  				- Autokonfiguration, Rekonfiguration



##### Public Domain Licence

- Software bei der

  - das Schutzrecht verfallen ist
  - auf das Schutzrecht verzichtet wird
  - das Konzept dem Gemeinwohl dient

- Was darf ich damit tun?

  - Jegliche Form der Nutzung ist erlaubt

    

##### Free Software/Freie Software

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Richard_Stallman_by_gisleh_01.jpg/220px-Richard_Stallman_by_gisleh_01.jpg"  />

- Free Software Foundation ->				 Richard Stallmann

- Copyleft
- Immer kostenlos
- Quelloffenheit



##### Open Source

- Verpflichtung zur Quelloffenheit
- Kommerzielle Agitation erlaubt
- GNU -> General Public License
  - immer quelloffen und immer kostenlos



##### Unterschied zwischen kommerzieller und proprietärer Software



##### Was sind die Ziele der Softwareentwicklung

- Wartbarkeit
- Portierbarkeit
- Performance
- Stabilität
- Benutzerfreundlichkeit
- Skalierbarkeit



##### Netzwerk-Topologien



<img src="Bilder/Netzwerktopologie.png" />







##### Datenübertragung in Netzwerken

###### Übertragungsmedien

<img src="Bilder/Ubertragungsmedien.png" />

​				<a href="https://www.elektronik-kompendium.de/sites/kom/0211192.htm"> Elektronikkompendium</a> : wichtige Informationen zu dem Thema

###### Übertragungsmodi

- Duplex-Arten



#####  Peer-to-Peer und Client-Server

- Peer-to-Peer
  - Alle Teilnehmer sind gleichberechtigt
  - Jeder Server kann Client und jeer Client auch Server sein
  - ein Client ist ein "Server" sobald er Dienste zur verfügung stellt
  - Vorteile :
    - Einfache Wartung
    - Einfache Infrastruktur
    - kostengünstig
  - Nachteile :
    - Bei steigender  Anzahl Teilnehmer -> Sicherheitsproblematik
    - keine zentrale Verwaltung



- Client-Server
  - Vorteile :
    - Zentrale Verwaltbarkeit
    - hohe Verfügbarkeit
    - hohe Datensicherheit
  - Nachteile :
    - Administration
    - höhere Kosten in Bezug auf Hardware und Software



##### Zugriffsverfahren in Netzwerken

- CSMA/CD (Carrier Sense Multiple Access/Collision Detection)  bzw CSMA/CA

- Versuch, Kollisionen in Netzwerken gering zu halten bzw. zu vermeiden

  

  <img src="Bilder/Csmacd.PNG" />



##### TCP/IP

- tree way Handshake

  <img src = "Bilder/threeway.png" />

  

- Verbindungsorientiert :

  - Verbindungsorientierte Anwendungen nutzen im Internet das Transportprotokoll TCP, um vor der eigentlichen Datenübertragung eine virtuelle Verbindung aufzubauen. Dabei werden im Laufe der Kommunikation die Phasen Verbindungsaufbau, Datenübertragung und Verbindungsabbau durchlaufen. Eine verbindungsorientierte Kommunikation ermöglicht es, übertragene IP-Pakete so abzusichern, daß eine wiederholte Übertragung fehlerhafter Pakete jederzeit möglich ist, da während des Datentransfers der Erfolg oder Mißerfolg einer Aktion gemeldet wird.

- Verbindungslos : 

  - Eine verbindungslose Kommunikation startet dagegen sofort mit der Übertragung von
    Daten, ohne daß der Empfänger seine Empfangsbereitschaft signalisiert
    haben muß, kann aber die angesprochenen Dienste der verbindungsorientierten
    Kommunikation nicht bieten. Im Internet dient dazu das Transportprotokoll UDP.



- paketorientierte Datenübermittlung
  - Standard-Aufbau von Paketen:
    - Trailer (Checksummen )	
    - Payload (Die Daten selbst)
    - Header (Wo gehts hin)
    - ...

- Leitungsvermittlung
  - durchgehend geschaltete Verbindungskanäle
  - Vermittlungsstellen
  - Einwahlverfahren
  - exklusive Datenverbindung in einem Übertragungskanal



##### IP4-Adresse

-  logische Adresse

- Netzwerkanteil, Hostanteil 

- Subnetze + Subnetzmasken

- Netzwerk-Klassen

  <img src="Bilder/netzwerkklassen.png" />



##### Firewall

- ein Sicherheitskonzept 
- Hier wird immer eine Security Policy umgesetzt (mehrschichtig)
- besteht aus mehreren Komponenten:
  - im Kern : Paketfilter
  - Proxy
  - physische Zugangskontrolle
  - Zugangskontrolle per biometrischer Daten
- Paketfilter : 
  - Wendet einen Satz von Regeln auf eingenehenden und ausgehenden Datenverkehr an
  - Worauf beziehen sich diese Regeln? :
    - Quelladresse und Zieladresse
    - Protokoll
    - Zielport und Quellport
  - Typen von Regel-Sätzen:
    - stateless
      - Auf alle Datenpakete werden immer dieselben Regeln angewendet, egal was vorher passiert ist
    - stateful
      - System ist in der Lage, selbst Regeln zu erzeugen und Zustände von Verbindungen zu speichern (Tabelle)
  - Intrusion Detection & Intrusion Prevention  :
    - "intelligente Systeme", welche versuchen, im Netzwerkverkehr durch Mustererkennung potentielle Angriffe zu erkennen/ zu verhindern
- Proxy Server:
  - Ein Dienst, welcher Stellvertretend für alle Clients im lokalen NW Anfragen im öffentlichen NW stellt
  - Man verbirgt damit die innere Struktur des lokalen Netzwerks nach außen hin
  - So ein Proxy kann diverse Filter haben (Anwendungsfilter, Filter für Webinhalte...)

##### VLAN

- Dienen der Auftrennung physikalisch gegebener Netzwerke in logische Netzwerke
- Ziele : 
  - Kosten
  - Sicherheit
  - Verwaltung, Arbeitsgruppenbildung...
- Jedes VLAN bildet eine eigene Broadcast-Domäne

##### 

##### Grundbegriffe der Netzwerktechnik

- Bandbreite 
  - Frequenz : Hz, Schwingung/s
- Datenübertragungsrate
  - digitale Datenmenge / Zeiteinheit

- Datendurchsatz 
  - Nettodatenmenge pro Zeit 





##### BIOS

			- Programm, welches die grundlegendsten Komponenten eines Rechner bei Systemstart ansteuert
			
			- Beinhaltet rudimentäre I/O und Grafiktreiber
			
			- Initiert das OS -> Bootloader		


​	
​	
​		
​			- Serieller Bus/Serielle Schnittstelle
​			
​				- Daten werden nacheinander über einen Kanal gesendet

##### Seriell vs Parallel(Bus)

			- Parallel/Parallele Schnittstelle
	
				- Nebeneinander, zusammengehörende Daten über Kanäle senden
					-> Vorteile:
				
					- Grundsätzlich schneller als seriell
				
				-> Nachteile:
				
					- Signal muss am Ende wieder zusammengesetzt werden
					
					- Störsignale bei hohem Datendurchsatz(elektromagnetische Strahlung)



##### Hardware-Komponenten

- Festplatten(Aufbau physikalisch, logisch)
- SSD
- Hauptkomponenten Mainboard

- RAM 
  - grundsätzlicher Aufbau
  - SRAM/DRAM
- ROM 

- USB
  - Kabellänge
  - Übertragungsgeschwindigkeiten



##### Bluetooth

- Wieviele Geräte können in einem Bluetooth-Piconet kommunizieren (8 bit adresse)
- Übertragungsgeschwindigkeiten 



##### Betriebssysteme 

- Grundsätzliche Unterschiede Windows/Linux

- Unix



##### VPN 

- Was ist ein VPN
  - Ein privates, geschlossenes (exklusives) Netzwerk, getunnnelte Direktverbindung
- Vorteile : 
  - Flexibilität
  - Kosteneinsparung
- Anforderung an ein VPN
  - Abhörsichere Verbindungen
  - Einhaltung von Datenintegrität
  - QoS
- End-to-End, End-to-Site, Site-to-Site

- End to End

<img src="Bilder/EndtoEnd.png" />

- Site to Site

<img src="Bilder/SitetoSite.png" />

- End to Site<img src="Bilder/EndtoSite.png" />					



##### Elektrotechnik

<img src="Bilder/elektrotechnik.png" /> 



##### Digitaltechnik

- boolsche Algebra
- logische Gatter

<img src="Bilder/digitech.png" />



- Warheitstabellen

- Duales Zahlensystem (1+1=10)

- Kondensatoren

  



#### SAN und NAS

###### NAS - Network attached storage

- Netzwerkspeicherlösung
- Zugriff auf einen Speicher übers NW
- Vorteile :
  - "Immer" erreichbar
  - skalierbar
  - kostengünstig
  - einfach zu adminstrieren
  - einfach einzurichten
- Nachteile :
  - kann zu überlastung des NW führen

###### SAN - Storage area network

- Cluster von Festplatten auf die per Fibrechannel zugegriffen werden kann
- Vorteile
  - hohe Geschwindigkeit
  - Ausfallsicherheit
  - arbeitet Gegen NW-Überlastung
  - redundante Datenhaltung
  - hochverfügbar
  - skaliert..
  - 
- Nachteile : 
  - teuer
  - komplex

##### NAT

- lokale Adressen werden auf eine öffentliche Adresse geroutet

- hat historische Gründe ->IPv4-Adressknappheit

- Router-Angelegenheit



#### RAID

- Ziele/Wofür RAID?

  - redundante Datenhaltung
  - Performancesteigerung beim Zugriff
  - Kosten einsparen
  - Gewisse Hochverfügbarkeit
- JBOD - just a bunch of Disks
- Die Level : 

  - Raid 0 (Striping) :
    - Striping führt zur Erhöhung der Transferraten beim Lesen/Schreiben
    - mindestanzahl Platten : 2
    - Bei n Platten ist die maximale nutzbare Kapazität = n
    - Keine Redundanz
  - Raid 1 (Mirroring) :
    - Nutzdaten werden identisch auf mehrere Platten gespeichert
    - Optimale Ausfallsicherheit -> 100% Redundanz
    - mindestanzahl Platten : 2
  - Raid 10 :
    - Kombination aus Striping und Mirroring
    - mindestanzahl an Platten : 4
    - Kapazität : Die Hälfte
  - Raid 5 :
    - Striping mit verteilter Parität
    - Mindestanzahl an Platten : 3
    - Nutzbare Kapazität : n - 1
- Software- und Hardware-RAID 





##### Betriebssystemtheorie 

- Prozess
  - Prozesszustände
- Threads
- Scheduler
  - Programm, welches unter der Kontrolle des OS läuft und Prozessen Zustände und Ressourcen 	   zuweist
  - Scheduler-Algorithmen





##### wichtige Netzwerkprotokolle

- Regelsammlung von Kommunikationsvorschriften



